
CONTENTS OF THIS FILE
---------------------

 * Installation
 * Dependencies

INSTALLATION
------------

1. Decompress the archive and copy the rename_modules folder into your modules
   folder.

     sites/all/modules

2. Install the Libraries 7.x-2.x module in your modules folder.

3. Download the jQuery replaceText library. The link is provided below.

   For advanced Drush 4.x+ users, type:

     drush dl-replacetext

   It will download and create the file and folder:

     sites/all/libraries/jquery_replacetext/jquery.ba-replacetext.min.js

4. Create a "jquery_replacetext" folder under libraries:

     sites/all/libraries/jquery_replacetext

5. Copy the jquery.ba-replacetext.min.js to this folder.

6. Turn on the module under:

     admin/modules

7. Your user role must have the "Administer permissions" and the
   "Administer Rename Modules" permission for your user role. Enable
   "View the administration theme" permission and the "Seven" theme
   for the best results for text replacement.

     admin/people/permissions

8. Navigate to:

     admin/config/user-interface/rename-modules

   Configure the settings to your liking. Rename and be happy!

NOTE:

It's a best practice to install this module early during site development
as such a change may confuse your users who may not like the change.

DEPENDENCIES
------------

Libraries 7.x-2.x-dev module
http://drupal.org/project/libraries

jquery-replacetext v1.1 library
http://benalman.com/projects/jquery-replacetext-plugin/
https://github.com/cowboy/jquery-replacetext
