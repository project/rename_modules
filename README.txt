
Are you or your users confused about Drupal's "Modules"? Do you wish you
could change the title to something better without breaking Drupal?

Rename Modules can fix this!

Here's a list of words you can rename it with:

  - Add-ons
  - Addons
  - Extensions
  - Extend (Menu) / Extension (Words)
  - Extras
  - Features
  - Plug-ins
  - Plugins

Features
------------

Select from a predefined list of words and use it to rename any text of
"Modules" within the administration interface.


Disclaimer
------------
It works by using Javascript to manipulate text in the browser. The change
is cosmetic and does not interfere with the operation of modules. URL
paths, page links and menu links that have /modules/ links will still be
present. There is no guarantee that it will correctly replace *all* text
references of "Modules" on a page. Please evaluate it to see if it works
well enough for you.

If you don't want a certain "modules" word to be replaced, wrap that word
with:

  <span class="no-touch">modules</span>


Known Issues
------------

- Don't rename the title of the "Modules" menu item under the "Management"
  menu. Otherwise, the title won't be replaced correctly.

- Currently, this module is written and tested for English. Kindly submit
  patches to the issue queue for other language support.

- Using the jQuery replaceText library does a decent job of not breaking
  layout but for dynamic HTML, it may cause problems.


Author
------------

Giovanni Glass
momendo@gmail.com
http://drupal.org/user/30654
http://twitter.com/momendo
http://www.giovanniglass.com
