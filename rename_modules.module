<?php

/**
 * @file
 * Implements a cosmetic rename of "module" strings
 * on administration pages and in the navigation menu.
 */

module_load_include('inc', 'rename_modules', 'rename_modules_admin');

/**
 * Implements hook_help().
 */
function rename_modules_help($path, $arg) {
  switch ($path) {
    case 'admin/config/user-interface/rename-modules':
      $output = '';
      $output .= t('<p>Rename <a href="@link">the menu link</a> and all text references of "<span class="no-touch">Modules</span>" in the administration interface. It does not affect menu paths, url paths or file paths.<br /> All changes are cosmetic.</p>', array('@link' => url('admin/structure/menu/item/' . _rename_modules_get_modules_info('mlid') . '/edit')));
      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function rename_modules_menu() {
  $items = array();

  $items['admin/config/user-interface/rename-modules'] = array(
    'title' => 'Rename modules',
    'description' => 'Rename "<span class="no-touch">Modules</span>"',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rename_modules_admin_settings'),
    'file' => 'rename_modules_admin.inc',
    'access arguments' => array('administer rename modules'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function rename_modules_permission() {
  $permissions = array(
    'administer rename modules' => array(
      'title' => t('Administer Rename <span class="no-touch">Modules</span>'),
      'description' => t('Change all text references of "<span class="no-touch">Modules</span>" on administration pages.'),
    ),
  );
  return $permissions;
}

/**
 * Implements hook_library().
 */
function rename_modules_library() {
  $module_path = drupal_get_path('module', 'rename_modules');

  // Main Javascript file.
  $libraries['rename_modules'] = array(
    'title' => 'Rename modules',
    'website' => 'http://drupal.org/handbook/modules/rename_modules',
    'version' => '1.0',
    'js' => array(
      $module_path . '/rename_modules.js' => array(),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_libraries_info().
 * From the libraries module.
 */
function rename_modules_libraries_info() {
  $libraries['jquery_replacetext'] = array(
    'title' => 'jQuery replaceText: String replace for your jQueries!',
    'vendor url' => 'http://benalman.com/projects/jquery-replacetext-plugin/',
    'download url' => 'https://github.com/cowboy/jquery-replacetext',
    'version arguments' => array(
      'file'    => 'jquery.ba-replacetext.min.js',
      'pattern' => '/jQuery replaceText\s+-\s+v([1-9\.]+)/',
      'lines'   => 3,
    ),
    'files' => array(
      'js' => array(
        'jquery.ba-replacetext.js',
      ),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array(
            'jquery.ba-replacetext.min.js',
          ),
        ),
        'variant arguments' => array(
          'variant' => 'minified',
        ),
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_init().
 * Load the requisite Javascript files if we're in the admin section.
 */
function rename_modules_init() {
  $current_path = current_path();
  if (path_is_admin($current_path) && variable_get('rename_modules_text')) {
    $name = 'jquery_replacetext';
    if (($library = libraries_detect($name)) && !empty($library['installed'])) {
      $replacement = array();
      $replacement['replacementWords'] = rename_modules_word_variations(variable_get('rename_modules_new_word', 0));
      drupal_add_js(array('rename_modules' => $replacement), 'setting');
      libraries_load($name, 'minified');
      drupal_add_library('rename_modules', 'rename_modules');
    }
    else {
      $error_message  = $library['error message'];
      $error_message .= ' ' . t('Please check if it is installed correctly.');
      drupal_set_message(check_plain($error_message), 'error');
    }
  }
}
