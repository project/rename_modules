<?php

/**
 * @file
 * Rename Modules Admin UI
 * Implements Rename Modules configuration screen.
 */

/**
 * An array of word replacements
 *
 * @param array $key
 *   A specific key in the array. Or the first key in array if no key found.
 *
 * @return array
 *   An array keyed by replacement words. The value is an array.
 *   The keys are javascript regular expressions search terms. The values are
 *   replacement terms.
 */
function rename_modules_word_variations($key = FALSE) {
  $words = array(
    'Add-ons' => array(
      'a module' => t('an add-on'),
      'Modules'  => t('Add-ons'),
      'Module'   => t('Add-on'),
      'modules'  => t('add-ons'),
      'module'   => t('add-on'),
    ),
    'Addons' => array(
      'a module' => t('an addon'),
      'Modules'  => t('Addons'),
      'Module'   => t('Addon'),
      'modules'  => t('addons'),
      'module'   => t('addon'),
    ),
    'Extensions' => array(
      'a module' => t('an extension'),
      'Modules'  => t('Extensions'),
      'Module'   => t('Extension'),
      'modules'  => t('extensions'),
      'module'   => t('extension'),
    ),
    'Extend (Menu) / Extension (Words)' => array(
      'SELECTOR'   => array(
        'selector' => 'a#toolbar-link-admin-modules',
        'search'   => 'Modules',
        'replace'  => 'Extend',
      ),
      'a module' => t('an extension'),
      'Modules'  => t('Extensions'),
      'Module'   => t('Extension'),
      'modules'  => t('extensions'),
      'module'   => t('extension'),
    ),
    'Extras' => array(
      'a module' => t('an extra'),
      'Modules'  => t('Extras'),
      'Module'   => t('Extra'),
      'modules'  => t('extras'),
      'module'   => t('extra'),
    ),
    'Features' => array(
      'Modules'  => t('Features'),
      'Module'   => t('Feature'),
      'modules'  => t('features'),
      'module'   => t('feature'),
    ),
    'Plug-ins' => array(
      'Modules'  => t('Plug-ins'),
      'Module'   => t('Plug-in'),
      'modules'  => t('plug-ins'),
      'module'   => t('plug-in'),
    ),
    'Plugins' => array(
      'Modules'  => t('Plugins'),
      'Module'   => t('Plugin'),
      'modules'  => t('plugins'),
      'module'   => t('plugin'),
    ),
  );

  if (is_string($key)) {
    if (array_key_exists($key, $words)) {
      return $words[$key];
    }
    else {
      // Return the first key if not found like 0.
      return key($words);
    }
  }

  return $words;
}

/**
 * @return array
 *   Keys of words and identical values.
 */
function _rename_modules_array_combine() {
  $words = rename_modules_word_variations();
  $keys  = array_keys($words);
  return array_combine($keys, $keys);
}

/**
 * @return object
 *   Find the system "Modules" menu link.
 */
function _rename_modules_find_modules_link() {
  $result = db_query_range("SELECT * FROM {menu_links} ml INNER JOIN {menu_router} m ON ml.router_path = m.path WHERE ml.menu_name = :menu AND ml.module = 'system' AND m.path = 'admin/modules' ORDER BY m.number_parts ASC", 0, 1, array(':menu' => 'management'), array('fetch' => PDO::FETCH_ASSOC))->fetch();
  return $result;
}

/**
 * Get the system "Modules" menu link.
 *
 * @param integer $mlid
 *   menu link id
 *
 * @return array
 *   The "Modules" link.
 */
function _rename_modules_get_modules_link($mlid = FALSE) {
  if (!$mlid) {
    $link = _rename_modules_find_modules_link();
    $mlid = $link['mlid'];
  }
  $link_load = menu_link_load($mlid);
  return $link_load;
}

/**
 * Get a value from key from the database.
 *
 * @param string $type
 *   Key of type.
 *
 * @return string
 *   Get the system "Modules" menu link in an array.
 */
function _rename_modules_get_modules_info($type) {
  $link = _rename_modules_get_modules_link();
  return $link[$type];
}

/**
 * @return array
 *   FormAPI callback.
 */
function rename_modules_admin_settings() {
  $form = array();

  $form['rename_modules_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#weight' => 0,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['rename_modules_fieldset']['rename_modules_new_word'] = array(
    '#type' => 'select',
    '#title' => t('Choose a new phrase'),
    '#default_value' => variable_get('rename_modules_new_word', rename_modules_word_variations(0)),
    '#options' => _rename_modules_array_combine(),
    '#multiple' => FALSE,
  );

  $form['rename_modules_fieldset']['rename_modules_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change all text references on administration pages. <em>Note: Turn off when using coder or devel <span class="no-touch">module</span> to prevent confusion.</em>'),
    '#default_value' => variable_get('rename_modules_text', TRUE),
  );

  $form = system_settings_form($form);

  return $form;
}
