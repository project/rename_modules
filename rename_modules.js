/**
 * @file
 * Renames "module" and "modules" on pages in the admin interface.
 */
(function ($) {

  Drupal.rename_modules = Drupal.rename_modules || {};

  /**
   * Find our words and rename them.
   */
  Drupal.behaviors.rename_modules = {
    attach: function (context, settings) {
      // Rename key "this" to value "that".
      // The key is a regular expression string.
      // Avoid menu edit, /coder/, and /devel/ modules to prevent confusion.
      if (!$(".page-devel").size()
          && !$(".page-admin-structure-menu-item-edit").size()
          && !$(".page-admin-config-development").size()) {
        $.each(settings.rename_modules.replacementWords, function(key, value) {
          // Process key. If SELECTOR, we process it as an object for CSS selector searches.
          if (key == 'SELECTOR') {
            $(value.selector)
              .replaceText(eval("/" + value.search +"/g"), value.replace);
          } else {
            $("body *:not(textarea,pre,code,span.no-touch,a[href$=/rename-modules],.page-admin-config-user-interface-rename-modules h1.page-title)")
              .replaceText(eval("/" + key +"/g"), value);
            }
        });
      }
    },
  }

}(jQuery));
