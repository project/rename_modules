<?php

/**
 * @file
 * drush integration for rename_modules.
 */

/**
 * Implements hook_drush_command().
 */
function rename_modules_drush_command() {
  $items = array();

  $items['dl-replacetext'] = array(
    'callback' => 'rename_modules_drush_replacetext_download',
    'description' => dt('Downloads the required replaceText jQuery plugin.'),
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * @param string $section
 *
 * @return string
 */
function rename_modules_drush_help($section) {
  switch ($section) {
    case 'drush:dl-replacetext':
      return dt("Downloads the required replaceText jQuery plugin.");
  }
}

/**
 * drush command callback. Download the jQuery Replacetext library.
 */
function rename_modules_drush_replacetext_download() {
  if (module_exists('libraries')) {
    $path = 'sites/all/libraries/jquery_replacetext';

    // Create the path if it does not exist.
    if (!is_dir($path)) {
      drush_op('mkdir', $path);
      drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
    }
  }
  else {
    $path = drupal_get_path('module', 'rename_modules');
  }
  // Download the plugin.
  if (_drush_download_file('http://github.com/cowboy/jquery-replacetext/raw/master/jquery.ba-replacetext.min.js', $path . '/jquery.ba-replacetext.min.js')) {
    drush_log(dt('jquery.ba-replacetext.min.js has been downloaded to @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download jquery.ba-replacetext.min.js to @path', array('@path' => $path)), 'error');
  }
}
